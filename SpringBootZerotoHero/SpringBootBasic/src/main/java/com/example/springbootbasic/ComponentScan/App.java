package com.example.springbootbasic.ComponentScan;

import com.example.springbootbasic.ComponentScan.others.otherGirl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/*
* khi không chỉ thị Bean được gọi thì SpringBoot vẫn tìm ra các Bean nằm cùng và các Bean nằm trong packages cùng App
* khi chỉ định sử dụng @ComponentScan thì chỉ tìm thấy Bean mà nó chỉ định
* */
@ComponentScan("com.example.springbootbasic.ComponentScan.others")
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(App.class, args);
        try {
            Girl girl = context.getBean(Girl.class);
            System.out.println("Bean: " + girl.toString());
        } catch (Exception e) {
            System.out.println("Bean Girl không tồn tại");
        }
        try {
            otherGirl otherGirl = context.getBean(otherGirl.class);
            System.out.println("Bean: " + otherGirl.toString());
        } catch (Exception e) {
            System.out.println("Bean Girl không tồn tại");
        }
    }
}
