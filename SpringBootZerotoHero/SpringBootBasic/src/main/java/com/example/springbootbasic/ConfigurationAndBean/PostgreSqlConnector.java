package com.example.springbootbasic.ConfigurationAndBean;

public class PostgreSqlConnector extends DataConnector {
    @Override
    public void connect() {
        System.out.println("Đã kết nối tới Postgresql: " + getUrl());
    }
}
