package com.example.springbootbasic.Component_Service_Repository.model;

public class Girl {
    String name;

    public Girl(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Girl{" +"name='" + name + '\'' +'}';
    }
}
